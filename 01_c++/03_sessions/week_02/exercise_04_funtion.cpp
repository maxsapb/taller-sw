/**
 * @file CodeReview.cpp
 * @author Maxs Paucar Barrientos (maxsapb@gmail.com)
 * @brief Excercise 04:
 	Un departamento de climatolog�a ha realizado recientemente su conversi�n al sistema m�trico. Dise�ar un algoritmo para realizar las siguientes conversiones:
a. Leer la temperatura dada en la escala Celsius e imprimir en su equivalente Fahrenheit (la f�rmula de conversi�n es "F=9/5 �C+32").
b. Leer la cantidad de agua en pulgadas e imprimir su equivalente en mil�metros (25.5 mm = 1pulgada.

 * @version 1.0
 * @date 04.12.2021
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>

using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float fahrenheit = 0;
float MillimetersWater = 0;
float celsius = 0;
float waterInches = 0;
 
 /*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void Calculate();
void CollectData();
void ShowResults();
float CalculateFahrenheit (float celsius);
float CalculateMillimetersWater (float waterInches);
 
 /*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
 void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================
void Calculate(){
	fahrenheit = CalculateFahrenheit (celsius);
	MillimetersWater = CalculateMillimetersWater (waterInches);
}
//=====================================================================================================
// Collect Data throught the terminal
void CollectData(){	
	cout<<"\n\r============	Insert data=============\n\r";
	cout<<"Ingrese la temperatura en grados celsius : ";
	cin>>celsius;
	cout<<"Ingrese la cantida de agua : ";
	cin>>waterInches;
}

//=====================================================================================================
void ShowResults(){
	//Display phrase result
	cout<<"\n\r===========Show results=========\n\r";
	cout<<"La temperatura en Fahrenheit es : "<< CalculateFahrenheit (celsius)<<"F";
	cout<<"\n\rLa cantidad de agua en milimetros es : "<<CalculateMillimetersWater(waterInches)<<"mm";
}
//=====================================================================================================
float CalculateFahrenheit ( float celsius){
 	return ((9 *celsius)/5 )+ 32;			//F= 9/ 5 C + 32
}
//=====================================================================================================
float CalculateMillimetersWater ( float waterInches){
	return (float)waterInches * 25.5;			//25.5 mm = 1 pulgada
}
//=====================================================================================================
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
