/**
 * @file CodeReview.cpp
 * @author Luis Arellano (luis.arellano09@gmail.com)
 * @brief Excercise 01:
 		Elabore un algoritmo que dados como datos  de
		entrada el radio y la altura de un cilindro calcular, el rea lateral y el volumen del cilindro.
		A = pi^2radio*altura	V =radio^2pi*altura
 * @version 1.0
 * @date 04.12.2021
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define PI 3.1416		// Constante Pi 

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float terminalInputHeight = 0.0; 		// Entrada de altura a traves del terminal
float terminalInputRadius = 0.0;	 	// Entrada de radio a traves del terminal

float circularArea = 0.0;				// Area circular calculada
float lateralAreaCylinder = 0.0;		// Area lateral del cilindro calculada
float volumeCylinder = 0.0;				// Volumen del cilindro calculada

/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void Calculate();
void ShowResults();
float CalculateCircularArea(float radius);
float CalculateLateralAreaCylinder(float radius, float height);
float CalculateVolumeCylinder(float radius, float height);

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/

void Run(){
	CollectData();
	Calculate();
	ShowResults();
}

//=====================================================================================================

// Collect Data throught the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"\tWrite the radius of the cylinder: ";
	cin>>terminalInputRadius;
	cout<<"\tWrite the height of the cylinder: ";
	cin>>terminalInputHeight;
}
//=====================================================================================================

void Calculate(){
	circularArea = CalculateCircularArea(terminalInputRadius);
	lateralAreaCylinder = CalculateLateralAreaCylinder(terminalInputRadius, terminalInputHeight);
	volumeCylinder = CalculateVolumeCylinder(terminalInputRadius, terminalInputHeight);
}
//=====================================================================================================

void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\tThe circular area is: "<< circularArea<<"\r\n";
	cout<<"\tThe lateral area is: "<<lateralAreaCylinder<<"\r\n";
	cout<<"\tThe volume is: "<<volumeCylinder<<"\r\n";
}
//=====================================================================================================

float CalculateCircularArea(float radius){
	return PI * pow(radius,2.0);	// area_circulo = pi*radio^2
}
//=====================================================================================================

float CalculateLateralAreaCylinder(float radius, float height){
	return 2.0 * PI * radius * height;	// area lateral = 2 * pi * radio * h
}

//=====================================================================================================
float CalculateVolumeCylinder(float radius, float height){
	return PI * pow(radius,2.0) * height;
}

 
