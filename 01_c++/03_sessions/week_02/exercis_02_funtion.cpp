/**
 * @file Template.cpp
 * @author Maxs Paucar
 * @brief File description
 * @version 1.0
 * @date 4/12/2021
 * 
 */
 /*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
 #include <iostream>

using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
 int women = 0;
 int men = 0;
 float Sum = 0;
 float PercentMen = 0;
 float PercentWomen = 0;
 
 
  
 /*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
 void Run();
 void Calculate();
 void CollectData();
 void ShowResults();
 int CalculateSum(int men,int women);
 float CalculatePercentMen(int men,int women);
 float CalculatePercentWomen(int men,int women);
  
 
 /*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 int main(){
	Run();			
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
 void Run(){
 	CollectData();
 	Calculate();
 	ShowResults();
 
}

//=====================================================================================================
void Calculate(){
	Sum =  CalculateSum(men,women);
	PercentMen = CalculatePercentMen(men,women);
	PercentWomen = CalculatePercentWomen(men,women);
	
}

//=====================================================================================================
// Collect Data throught the terminal
void CollectData(){	
	cout<<"========= Insert Data ===========\r\n";
	cout<<"Introdusca numero de hombres: ";
	cin>>men;
	cout<<"Introdusca el numero de mujeres: ";
	cin>>women;
}
//=====================================================================================================
void ShowResults(){
	cout<<"\r\n========= Show Results ===========\r\n";
	cout<<"\n\r===========show results=============\n\r";
	cout<<"El porcentaje de varones es: "<<CalculatePercentMen(men,women);
	cout<<"%";
	cout<<"\n\rEl porcentaje de mujeres es: "<<CalculatePercentWomen(men,women);
	cout<<"%";
}
//=====================================================================================================
int CalculateSum(int men,int women){
	return (men + women);															//calulate sum men and women
}

//=====================================================================================================
float CalculatePercentMen(int men,int women){
	return (float)men / (float)CalculateSum(men,women) * 100.0 ;					//calculate of percent men
}

//=====================================================================================================
float CalculatePercentWomen(int men,int women){
	return (float)women / (float)CalculateSum(men,women) * 100.0  ;					//Calculate of percent women
}




























