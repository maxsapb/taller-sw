/**
 * @file CodeReview.cpp
 * @author Maxs Paucar Barrientos (maxsapb@gmail.com)
 * @brief Excercise 04:
 	El costo de un autom�vil nuevo para un comprador es la suma total del costo del veh�culo, del porcentaje de la ganancia del vendedor y de los 
	impuestos locales o estatales aplicables (sobre el precio de venta). Suponer una ganancia del vendedor del 12% en todas las unidades y un impuesto del 6% y dise�ar un algoritmo para 
 	leer el costo total del autom�vil e imprimir el costo para el consumidor.

 * @version 1.0
 * @date 04.12.2021
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>

using namespace std;
/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
float cost = 0;
float CalculateFinalCost (cost)



 /*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
void Run();
void CollectData();
void ShowResults();
float CalculateFinalCost();


 /*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
 
int main(){
	
	Run();		
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
void Run(){
	CollectData();
	float CalculateFinalCost (cost);
	ShowResults();
}

//=====================================================================================================
// Collect Data throught the terminal
void CollectData(){	
	cout<<"\n\r============	Insert data=============\n\r";
	cout<<"Ingrese el costo del automovil del automovil : ";
	cin>>cost;
}

//=====================================================================================================
void ShowResults(){
	//Display phrase result
	cout<<"\n\r===========Show results=========\n\r";
	cout<<"El costo del auto para el consumidor  es : "<< CalculateFinalCost (cost);
}

//=====================================================================================================
float CalculateFinalCost (float cost){
 	return cost + (12/100) CalculateFinalCost + (6/100) CalculateFinalCost	;
}
	






