/**
 * @file Template.cpp
 * @author Maxs Paucar
 * @brief File description
 * @version 1.0
 * @date 4/12/2021
 * 
 */


/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
#include <math.h>

using namespace std;

/*******************************************************************************************************************************************
 *  												DEFINE
 *******************************************************************************************************************************************/
#define pi 3.1416			//constant pi


/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/

int main () {
	
	//declaration of variable and initialize
	float height = 0;							//input height of cylinder
	float rad = 0;								//input radio of cylinder
	float lateralArea = 0;						//cylinder lateral area
	float volume = 0;							//cylinder volume
	
	//display phrase height
	cout<<"\n\r=========insert data=============\n\r";
	cout<<"\n\rintrodusca la altura del cilindro: ";
	cin>>height;
	cout<<"introdusca el radio del cilindro: ";
	cin>>rad;
	
    //calculation
	lateralArea = 2 * pi * rad * height;		//Lateral area = 2 * pi * rad * height
	volume = pi * pow(rad,2) * height;			//Volume = pi * rad^ * height
	
	//Display phrase result
	cout<<"\n\r========show results=============\n\r";
	cout<<"El area lateral del cilindro es: "<<lateralArea;
	cout<<"\n\rEl volumen del cilindro es: "<<volume;	
	
	
	
	return 0;
}



