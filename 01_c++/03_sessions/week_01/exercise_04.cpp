
/**
 * @file Template.cpp
 * @author Maxs Paucar
 * @brief File description
 * @version 1.0
 * @date 4/12/2021
 * 
 */



/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>
using namespace std;

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main () {
	
	//declaration of variable and initialize of variable
	float celsius = 0;								//input temperature in celsius
	float waterInches = 0;                          //input water in inches    
	float fahrenheit = 0;							//output fahrenheits
	float millimetersWater = 0;						//output water in millimeters
	
	//Display initial phrase
	cout<<"\n\r============	Insert data=============\n\r";
	cout<<"Ingrese la temperatura en grados celsius : ";
	cin>>celsius;
	cout<<"Ingrese la cantida de agua : ";
	cin>>waterInches;
	
	//calculation
	fahrenheit = (9 / 5 * celsius) + 32;			//F= 9/ 5 C + 32
	millimetersWater = waterInches / 25.5;			//25.5 mm = 1 pulgada
	
	//Display phrase result
	cout<<"\n\r===========Show results=========\n\r";
	cout<<"La temperatura en Fahrenheit es : "<< fahrenheit<<"F";
	cout<<"\n\rLa cantidad de agua en milimetros es : "<<millimetersWater<<"mm";
	
	
	return 0;
	
}
