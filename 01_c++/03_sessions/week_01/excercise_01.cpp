/**
 * @file Template.cpp
 * @author Maxs Paucar
 * @brief File description
 * @version 1.0
 * @date 4/12/2021
 * 
 */
/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>

/*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
using namespace std;
int main () {
	
	//declaration and initialize of variable
	int men = 0.0;					//Entrada de n�mero de varones
	int women = 0.0;				//Entrada de n�mero de mujeres
	int sum = 0.0;					//Entrada de sumatoria varones con mujeres
	float percentWomen = 0.0;		// Porcentaje de mujeres
	float percentMen = 0.0;			//Porcentaje de varones
	
	//Display phrase_number of men
	cout<<"\n\r==========insert data=========\n\r";
	cout<<"Introdusca n�mero de hombres: ";
	cin>>men;
	cout<<"Introdusca el n�mero de mujeres: ";
	cin>>women;
	
	//calculation
	sum = (men + women);
	percentMen = (float)men / (float)sum * 100.0 ;				//calculate of percent men
	percentWomen = (float)women / (float)sum * 100.0  ;			//Calculate of percent women
	
	//Display phrase_result
	cout<<"\n\r===========show results=============\n\r";
	cout<<"El porcentaje de varones es: "<<percentMen;
	cout<<"%";
	cout<<"\n\rEl porcentaje de mujeres es: "<<percentWomen;
	cout<<"%";
	
	return 0.0;
}
