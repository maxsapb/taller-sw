
/**
 * @file Template.cpp
 * @author Maxs Paucar
 * @brief File description
 * @version 1.0
 * @date 4/12/2021
 * 
 */

/*******************************************************************************************************************************************
 *  												INCLUDE
 *******************************************************************************************************************************************/
#include <iostream>

using namespace std;

/*******************************************************************************************************************************************
 *  												GLOBAL VARIABLES
 *******************************************************************************************************************************************/
int sum = 0;
	//declaration of variable and initialize of variable
	int numberOfDisapproved = 0;					//input  number of Disapproved
	int numberOfApproved = 0;						//input  number of approved
	int numberOfOutstanding = 0;					//input  number of outstanding students
	int numberOfNotabl = 0;							//input  number of notable students
	int studentsWhoPassed = 0;						//sum of students who passed
	float percentStudentsWhoPassed = 0;				//percent of students who passed
	float percentOfDisapproved = 0;					//percent of students who disapproved
	float percentOfApproved = 0;   					//percent of students who approved
	float percentOfNotable = 0;						//percent of notable students
	float percentOfOutstanding = 0;					//percent of Outstanding students




/*******************************************************************************************************************************************
 *  												FUNCTION DECLARATION
 *******************************************************************************************************************************************/
 void initial ()
 int studentsWhoPassed (int numberOfApproved, int numberOfOutstanding, int numberOfNotabl);
 float percentStudentsWhoPassed (float studentsWhoPassed, float sum);
 float percentOfDisapproved (float numberOfDisapproved, float sum);
 float percentOfApproved (float numberOfApproved, float sum);
 float percentOfNotable (float numberOfNotabl, float sum);
 float percentOfOutstanding (float numberOfOutstanding, float sum);
 void results ();
 void Run();
void Calculate();
void CollectData();
void ShowResults();
 
 /*******************************************************************************************************************************************
 *  												MAIN
 *******************************************************************************************************************************************/
int main () {
	
	void Run();
	
	
	return 0;
}

/*******************************************************************************************************************************************
 *  												FUNCTION DEFINITION
 *******************************************************************************************************************************************/
void Run(){
	initial ();
	Calculate();
	results ()
}


 //=====================================================================================================
void Calculate(){
	S = Sum (numberOfDisapproved, numberOfApproved,numberOfOutstanding,numberOfNotabl)
	passed = studentsWhoPassed (numberOfApproved, numberOfOutstanding, numberOfNotabl);
	PPassed = percentStudentsWhoPassed (studentsWhoPassed, sum);
	Pdisapproved = percentOfDisapproved (numberOfDisapproved, sum);
	PApproved = percentOfApproved (numberOfApproved, sum);
	PNotable = percentOfNotable (numberOfNotabl,  sum);
	POutstanding = percentOfOutstanding ( numberOfOutstanding, sum);

}

 //=====================================================================================================
void initial () {

	//Display initial phrase 
	cout<<"\n\r==============Insert data======================\n\r";
	cout<<"Introduce el n�mero de desaprobados :";
	cin>>numberOfDisapproved;
	cout<<"Introduce el n�mero de aprobados: ";
	cin>>numberOfApproved;
	cout<<"Introduce el numero de alumnos sobresalientes: ";
	cin>>numberOfOutstanding;
	cout<<"Introduce el n�mero de alumnos notables: ";
	cin>>numberOfOutstanding;
}


 //=====================================================================================================
	int Sum (int numberOfDisapproved, int numberOfApproved, int numberOfOutstanding, int numberOfNotabl) {
	return numberOfDisapproved + numberOfApproved + numberOfOutstanding + numberOfNotabl;		//Total sum

 } 
	
 //=====================================================================================================
 	//calculation
	int studentsWhoPassed (int numberOfApproved, int numberOfOutstanding, int numberOfNotabl) {
	return 	numberOfApproved + numberOfOutstanding + numberOfNotabl;				//Total sum of students who passed
	

} 
 	
	   
 //=====================================================================================================	
	float percentStudentsWhoPassed (float studentsWhoPassed, float sum){
	return studentsWhoPassed / sum  * 100.0;					//Percent of students who passed

}
	

 //=====================================================================================================
	float percentOfDisapproved (float numberOfDisapproved, float sum){
	return numberOfDisapproved / sum * 100.0; 					//Percent of students who disapproved
}
	
 //=====================================================================================================
	float percentOfApproved (float numberOfApproved, float sum){
	return numberOfApproved / sum * 100.0;							//Percent of students who approved
}
 //=====================================================================================================
	float percentOfNotable (float numberOfNotabl, float sum){

	return numberOfNotabl / sum * 100.0;								//Percent of students notable
}

 //=====================================================================================================
	float percentOfOutstanding (float numberOfOutstanding, float sum){

	return numberOfOutstanding / sum * 100.0;						//Percent of students outstanding
}
 
  //=====================================================================================================
 	void results () {
	 
	 //show results in terminal
	cout<<"\n\r=================show results======================\n\r";
	cout<<"El porcentaje de alumnos que superaron el curso es :"<<percentStudentsWhoPassed<<"%";
	cout<<"\n\rEl porcentaje de alumnos que desaprobaron fue de : " <<percentOfDisapproved<<"%";
	cout<<"\n\rEl porcentaje de alumnos que aprobaron fue de :"<<percentOfApproved<<"%";
	cout<<"\n\rEl porcentaje de alumnos notables fue de :"<<percentOfNotable <<"%";
	cout<<"\n\rEl porcentaje de alumnos sobresalientes es de :"<<percentOfOutstanding<<"%";
}
 
